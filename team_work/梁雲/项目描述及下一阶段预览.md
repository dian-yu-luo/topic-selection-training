# 大创任务描述

## 任务目标

## 项目可行性

知识图谱的构建重点是对知识的定义，对知识的描述和可视化。

在项目调研之前，我们能看到社会对知识图谱的应用方案都是构建针对人的知识图谱，利用大数据提供个性化服务。

同时有领域知识图谱，例如，[农业](#农业)，工业

但是大部分的研究都是处于英文的数据抽取，比较著名的有bert模型，针对中文的抽取和训练都有欠缺。

而我们要做的是抽取有关地铁的数据和信息，生成独特的地铁数据信息

## 时间点分配

## 当前获得的数据，

数据来源：



## 处理原始数据代码

附录：代码的量最好在500~1000行，便于理解

关键词：**三元组**，**数据抽取**

github代码链接：

* 实践抽取综述[xiaoqian19940510/Event-Extraction: 近年来事件抽取方法总结，包括中文事件抽取、开放域事件抽取、事件数据生成、跨语言事件抽取、小样本事件抽取、零样本事件抽取等类型，DMCNN、FramNet、DLRNN、DBRNN、GCN、DAG-GRU、JMEE、PLMEE等方法 (github.com)](https://github.com/xiaoqian19940510/Event-Extraction)
* 教学项目[Roshanson/TextInfoExp: 自然语言处理实验（sougou数据集），TF-IDF，文本分类、聚类、词向量、情感识别、关系抽取等 (github.com)](https://github.com/Roshanson/TextInfoExp)
* 语言处理综述[fighting41love/funNLP: 中英文敏感词、语言检测、中外手机/电话归属地/运营商查询、名字推断性别、手机号抽取、身份证抽取、邮箱抽取、中日文人名库、](https://github.com/fighting41love/funNLP)
* <span id="农业"> </span>领域知识图谱[zhangyqCS/KnowledgeGraph_Agriculture: 农业领域知识图谱的构建，包括数据爬取(百度百科)、数据分类、利用结构化数据生成三元组、非结构化数据的分句(LTP)，分词(jieba)，命名实体识别(LTP)、基于依存句法分析(主谓关系等)的关系抽取和利用neo4j生成可视化知识图谱 (github.com)](https://github.com/zhangyqCS/KnowledgeGraph_Agriculture)
* 优秀的预训练模型https://github.com/dbiir/UER-py

## 获得的需求

## 论文基础

https://www.paddlepaddle.org.cn/modelbase

## 路径设计

先将能够跑通的代码跑通，查阅结束的效果

