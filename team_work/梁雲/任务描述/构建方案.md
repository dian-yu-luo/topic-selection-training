# 构建方案

### 前言



### 明确任务

1. 人物画像的知识图谱构建
2. 非结构化数据的结构化

### 两个重点

1. 非结构化数据结构化

### 实现方案

1. [Pelhans/Z_knowledge_graph: Bulding kg from 0 (github.com)](https://github.com/Pelhans/Z_knowledge_graph)
   1. 一个完善的非结构数剧的实体抽取完整的流程
   2. 缺点，使用语言不同
2. https://www.infoq.cn/article/POTuVeAk0Lh0VmW9T9a0
   1. 华为的构建方案
3. https://m.hanspub.org/journal/paper/36030
   1. 用户画像的构建方案
4. https://zhuanlan.zhihu.com/p/110953022
   1. 简介用户画像

### 举例

结构化抽取：

她是一位25岁的白领，211大学毕业，现在从事于互联网行业的设计工作，居住在北京。单身，平时喜爱摇滚乐，喜爱日本料理。收入中等。

### 推荐系统

1. 必须有数据支持
2. 比较优秀的推荐算法
   1. 协同过滤
   2. 标签化

### 后记

我们这个学期要做什么？实现工程代码？还是单纯的为了选题训练的结题报告

